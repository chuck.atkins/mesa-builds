FROM centos:6
MAINTAINER Chuck Atkins <chuck.atkins@kitware.com>

RUN yum install -y tar xz wget 

# Install development tools and X libraries
RUN yum install -y \
    git libtool automake libX11-devel libXext-devel libxcb-devel \
    bison flex

# Install a newer set of compilers from the Software Collections repos
RUN yum install -y centos-release-scl && \
  yum install -y \
    devtoolset-4-gcc devtoolset-4-gcc-c++ python27 python27-pip

# Cleanup
RUN yum clean all

RUN mkdir -p /opt

# Install the most recent CMake release
RUN cd /opt && \
  wget https://cmake.org/files/v3.6/cmake-3.6.1-Linux-x86_64.tar.gz && \
  tar -xvf cmake-3.6.1-Linux-x86_64.tar.gz && \
  rm -f cmake-3.6.1-Linux-x86_64.tar.gz

# Build and install LLVM
RUN cd /opt && \
  wget http://llvm.org/releases/3.8.1/llvm-3.8.1.src.tar.xz && \
  tar -xvf llvm-3.8.1.src.tar.xz && \
  mkdir llvm-3.8.1.bld && \
  cd llvm-3.8.1.bld && \
  /usr/bin/scl enable devtoolset-4 python27 -- \
    /opt/cmake-3.6.1-Linux-x86_64/bin/cmake \
      -DCMAKE_BUILD_TYPE=Release \
      -DCMAKE_INSTALL_PREFIX=/opt/llvm-3.8.1 \
      -DLLVM_BUILD_LLVM_DYLIB=ON \
      -DLLVM_ENABLE_RTTI=ON \
      -DLLVM_TARGETS_TO_BUILD=X86 \
      -DLLVM_INSTALL_UTILS=ON \
      /opt/llvm-3.8.1.src && \
  /usr/bin/scl enable devtoolset-4 python27 -- \
    make -j$(grep -c "^processor" /proc/cpuinfo) install && \
  cd /opt && \
  rm -rf llvm-3.8.1.src.tar.xz llvm-3.8.1.src llvm-3.8.1.bld \
    cmake-3.6.1-Linux-x86_64
  
# Install Mako templates (needed for Mesa)
RUN cd /opt && \
  wget https://pypi.python.org/packages/7a/ae/925434246ee90b42e8ef57d3b30a0ab7caf9a2de3e449b876c56dcb48155/Mako-1.0.4.tar.gz && \
  tar -xvf Mako-1.0.4.tar.gz && \
  rm -f Mako-1.0.4.tar.gz

# Install the girder client for uploading results
RUN /usr/bin/scl enable python27 -- pip install girder-client

# Add the Mesa build script
ADD build-mesa.sh /opt/build-mesa.sh
RUN chmod +x /opt/build-mesa.sh

# Setup the shared mount point
RUN mkdir /mnt/shared

# Run the Mesa build script with new compilers enabled
ENTRYPOINT /usr/bin/scl enable devtoolset-4 python27 /opt/build-mesa.sh
