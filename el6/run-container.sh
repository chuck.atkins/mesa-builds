#!/bin/bash

GIRDER_ARGS=""
if [ -n "${GIRDER_TOKEN}" ]
then
  GIRDER_ARGS="-e GIRDER_TOKEN=${GIRDER_TOKEN}"
fi
if [ -n "${GIRDER_DESTINATION}" ]
then
  GIRDER_ARGS="${GIRDER_ARGS} -e GIRDER_DESTINATION=${GIRDER_DESTINATION}"
fi

docker run \
  --volume="${PWD}:/mnt/shared:rw" \
  -e HOSTUID=$(id -u) -e HOSTGID=$(id -g) ${GIRDER_ARGS} \
  mesa-el6-build 
